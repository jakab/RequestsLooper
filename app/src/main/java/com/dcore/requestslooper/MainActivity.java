package com.dcore.requestslooper;

import android.net.TrafficStats;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.dcore.requestslooper.network.Operations;
import com.dcore.requestslooper.network.Post;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int FIRST_SYS_CPU_COLUMN_INDEX = 2;

    private static final int IDLE_SYS_CPU_COLUMN_INDEX = 5;

    Button btnStartStop;
    EditText txtInterval;
    private Operations operations;
    Disposable poolingDisposable;
    TextView tx,rx, txtTimer, txtCpu;
    private long mStartRX = 0;
    private long mStartTX = 0;
    private Timer timer;
    private Timer cpuTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnStartStop = (Button) findViewById(R.id.btn_start_stop);
        txtInterval = (EditText) findViewById(R.id.txt_interval);
        tx= (TextView) findViewById(R.id.txt_tx);
        rx= (TextView) findViewById(R.id.txt_rx);
        txtTimer = (TextView) findViewById(R.id.txt_timer);
        txtCpu = (TextView) findViewById(R.id.txt_cpu);
        operations = new Operations();
        btnStartStop.setOnClickListener(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putLong("mStartRX",mStartRX);
        outState.putLong("mStartTX",mStartTX);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mStartTX = savedInstanceState.getLong("mStartTX");
        mStartRX = savedInstanceState.getLong("mStartRX");
        if(poolingDisposable!=null){
            btnStartStop.setText("STOP");
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_start_stop:
                Integer interval = null;
                try {
                    interval = Integer.parseInt(txtInterval.getText().toString());
                } catch (Exception e) {

                }
                if (interval != null) {
                    mStartRX = TrafficStats.getTotalRxBytes();
                    mStartTX = TrafficStats.getTotalTxBytes();

                    if (poolingDisposable == null) {
                        btnStartStop.setText("STOP");
                        startTimer();
                        startCpuTimer();
                        poolingDisposable = getPoolingObservable(interval)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribeWith(new DisposableObserver<Post>() {
                                    @Override
                                    public void onNext(Post post) {
                                        tx.setText(String.valueOf(TrafficStats.getTotalTxBytes()-mStartTX));
                                        rx.setText(String.valueOf(TrafficStats.getTotalRxBytes()-mStartRX));
                                    }

                                    @Override
                                    public void onError(Throwable e) {
                                        Log.e(getClass().getName(), e.toString());
                                    }

                                    @Override
                                    public void onComplete() {

                                    }
                                });
                    } else {
                        btnStartStop.setText("START");
                        poolingDisposable.dispose();
                        poolingDisposable = null;
                        timer.cancel();
                        timer.purge();
                        cpuTimer.cancel();
                        cpuTimer.purge();
                    }
                }
                break;
        }
    }

    private void startCpuTimer() {
        cpuTimer = new Timer();
        cpuTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (poolingDisposable!=null) {
                            txtCpu.post(new Runnable() {
                                @Override
                                public void run() {
                                    String[]line = readProcessStat(android.os.Process.myPid()).split(" ");
                                    txtCpu.setText(line[38]);
                                }
                            });
                        } else {
                            cpuTimer.cancel();
                            cpuTimer.purge();
                        }
                    }
                });
            }
        }, 0, 100);
    }

    private void startTimer() {
        txtTimer.setText("00:00");
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (poolingDisposable!=null) {
                            txtTimer.post(new Runnable() {
                                @Override
                                public void run() {
                                    txtTimer.setText(getStringFromMilis(getMilisFromMinAndSecs(txtTimer.getText().toString()) + 1000));
                                }
                            });
                        } else {
                            timer.cancel();
                            timer.purge();
                        }
                    }
                });
            }
        }, 1000, 1000);
    }

    private Observable<Post> getPoolingObservable(Integer interval) {
        return Observable
                .interval(0, interval, TimeUnit.SECONDS)
                .switchMap(new Function<Long, ObservableSource<Post>>() {
                    @Override
                    public ObservableSource<Post> apply(@NonNull Long aLong) throws Exception {
                        return operations.getPost();
                    }

                });
    }

    private String getStringFromMilis(int milis) {
        return String.format("%02d:%02d",
                TimeUnit.MILLISECONDS.toMinutes(milis),
                TimeUnit.MILLISECONDS.toSeconds(milis) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(milis))
        );
    }

    private int getMilisFromMinAndSecs(String string) {
        String[] strings = string.split(":");
        int mins = Integer.parseInt(strings[0]);
        int secs = Integer.parseInt(strings[1]);
        return (int) TimeUnit.SECONDS.toMillis(secs + mins * 60);
    }

    /** Return the first line of /proc/pid/stat or null if failed. */
    public String readProcessStat(int pid) {

        RandomAccessFile reader = null;
        String line = null;

        try {
            reader = new RandomAccessFile("/proc/" + pid + "/stat", "r");
            line = reader.readLine();
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            try {
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return line;
    }
}
