package com.dcore.requestslooper.network;

import io.reactivex.Observable;
import retrofit2.http.GET;

/**
 * Created by jakab on 04/10/2017.
 */

public interface API {

    @GET("posts/1")
    Observable<Post> getPost();
}
