package com.dcore.requestslooper.network;

/**
 * Created by jakab on 04/10/2017.
 */

public class Resource {
    private static final Object sInstanceLock = new Object();
    private static API sINSTANCE = null;

    public static API getInstance() {
        if (sINSTANCE == null) {
            synchronized (sInstanceLock) {
                if (sINSTANCE == null)
                    sINSTANCE = BaseApiClient.getInstance().createService(API.class);
            }
        }

        return sINSTANCE;
    }
}
