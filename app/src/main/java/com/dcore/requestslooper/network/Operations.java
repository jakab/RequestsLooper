package com.dcore.requestslooper.network;

import io.reactivex.Observable;

/**
 * Created by jakab on 04/10/2017.
 */

public class Operations {

    public Observable<Post> getPost() {
        return Resource.getInstance()
                .getPost();
    }
}
