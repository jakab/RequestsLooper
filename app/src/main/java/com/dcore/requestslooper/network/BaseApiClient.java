package com.dcore.requestslooper.network;

import okhttp3.logging.HttpLoggingInterceptor;

/**
 * Created by jakab on 04/10/2017.
 */

public class BaseApiClient {
    private static ApiClient sINSTANCE = null;
    private static final Object sInstanceLock = new Object();

    public static ApiClient getInstance() {
        if (sINSTANCE == null) {
            synchronized (sInstanceLock) {
                if (sINSTANCE == null) {
                    HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
                    loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
                    sINSTANCE = new ApiClient();
                    sINSTANCE.getAdapterBuilder().baseUrl("https://jsonplaceholder.typicode.com/");
                    sINSTANCE.getOkBuilder()
                            .addInterceptor(loggingInterceptor);
                }
            }
        }
        return sINSTANCE;
    }
}
